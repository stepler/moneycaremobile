(function() {
  'use strict';

  document.querySelector('html').className =
    localStorage.getItem('theme') || 'blue';

  var loader = (function(){
    var resourses = [];

    var add = function(element, attrs) {
      resourses.push([element, attrs]);
    };

    var load = function(onDoneCallback) {
      var loadedCount = 0;
      var totalCount = resourses.length;

      resourses.forEach(function(item) {
        var element = item[0];
        var attrs = item[1];
        var node = document.createElement(element);
        for (var attr in attrs) {
          if (attr[0] === '_') {
            node[attr.slice(1)] = attrs[attr];
          }
          else {
            node.setAttribute(attr, attrs[attr]);
          }
        }
        node.onload = function() {
          loadedCount += 1;
          if (loadedCount === totalCount) {
            onDoneCallback()
          }
        };
        document.head.appendChild(node);
      });
      resourses = [];
    };

    return {add:add, load:load}
  })();

  var isOffline = !window.navigator.onLine;
  var applicationCache = window.applicationCache;

  var loadApp = function(){
    loader.add('link', {
      'type': 'text/css',
      'rel':  'stylesheet',
      'href': '/static/build/styles.css'
    });
    loader.add('script', {
      '_async': false,
      'src': '/static/build/vendor.js'
    });
    loader.add('script', {
      '_async': false,
      'src': '/static/build/app.js'
    });

    loader.load(function() {
      setTimeout(function(){
        var bootstrap = document.querySelector("#bootstrap");
        if (!bootstrap) { return }
        bootstrap.classList.add('hiding');
        setTimeout(function(){
          bootstrap.remove();
        }, 500);
      }, 1000);
    });
  };

  if (isOffline) {
    loadApp();
    return;
  }

  applicationCache.addEventListener('noupdate', function(e) {
    loadApp();
  }, false);

  applicationCache.addEventListener('updateready', function(e) {
    applicationCache.swapCache();
    loadApp();
  }, false);

  applicationCache.addEventListener('cached', function(e) {
    loadApp();
  }, false);

  applicationCache.addEventListener('error', function(e) {
    loadApp();
  }, false);

})();
