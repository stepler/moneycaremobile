export const {
  APP_HOST = 'moneycare.me',
  NODE_ENV = 'production'
} = process.env;
