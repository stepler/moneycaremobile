import _ from 'lodash';
import {NODE_ENV} from '../conf';

const isDebugEnambled =
  _.isObject(process) &&
  _.isObject(process.env) &&
  NODE_ENV === 'development';

const matchDebugKey = (key) => {
  return window.location.hash.indexOf('_k='+key) > -1;
};

const debug = (key) => {
  if (isDebugEnambled) {
    return matchDebugKey(key);
  }
  return false;
};

export {
  debug
}
