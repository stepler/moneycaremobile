
const getTranslateX = (node) => {
  let values;
  let transform = getComputedStyle(node).transform;
  if (!(transform && /\(/.test(transform))) {
    return 0;
  }

  values = transform.split('(')[1];
  values = values.split(')')[0];
  values = values.split(',');

  return parseInt(values[4] || 0);
};

const getWidth = (node) => {
  let width = getComputedStyle(node).width;

  return parseInt(width || '0');
};

const setSwipe = (node) => {
  let startX = 0;
  let startY = 0;
  let currentX = 0;
  let currentY = 0;
  let currentTranslate = 0;
  let isScrollStarted = false;
  let isGestureStarted = false;
  let isTextZoomStarted = false;
  let detectThreshold = 2;
  let gestureThreshold = 50;
  let textZoomTimeout = null;

  const onDown = (e) => {
    isScrollStarted = false;
    isGestureStarted = false;
    isTextZoomStarted = false;

    currentTranslate = getTranslateX(node);
    currentX = startX = e.targetTouches[0].pageX;
    currentY = startY = e.targetTouches[0].pageY;

    node.classList.add('transition-disabled');

    textZoomTimeout = setTimeout(() => {
      isTextZoomStarted = true;
    }, 1000);
  };

  const onMove = (e) => {
    if (textZoomTimeout) {
      clearTimeout(textZoomTimeout);
    }

    currentX = e.targetTouches[0].pageX;
    currentY = e.targetTouches[0].pageY;

    if (!isScrollStarted && !isGestureStarted) {
      const diffX = Math.abs(startX - currentX);
      const diffY = Math.abs(startY - currentY);
      if (diffX > detectThreshold || diffY > detectThreshold) {
        isGestureStarted = diffX > diffY;
        isScrollStarted = !isGestureStarted;
      }
    }
    if (isGestureStarted && !isTextZoomStarted) {
      e.preventDefault();
      e.stopPropagation();

      requestAnimationFrame(() => {
        if (!isGestureStarted) { return; }
        const offset = currentTranslate + currentX - startX;
        node.style.transform = `translateX(${offset}px)`;
      })
    }
  };

  const onUp = (e) => {
    node.classList.remove('transition-disabled');
    clearTimeout(textZoomTimeout);

    if (isGestureStarted && !isTextZoomStarted) {
      e.preventDefault();
      e.stopPropagation();

      let offset = currentTranslate;
      if (Math.abs(currentX - startX) > gestureThreshold) {
        const direction = currentX > startX ? 'left' : 'right' ;
        offset = (direction === 'left') ? 0 : getWidth(node) * -1 ;
      }
      node.style.transform = 'translateX('+offset+'px)';
    }
  };

  for (const child of node.childNodes) {
    child.addEventListener('touchstart', onDown);
    child.addEventListener('touchmove', onMove);
    child.addEventListener('touchend', onUp);
    child.addEventListener('touchcancel', onUp);
  }
};

const resetSwipe = (node) => {
  node.style.transform = 'translateX(0px)';
};

const openPage = (component, uri, options) => {
  options = options || {};
  let classMethod = options.backward ? 'add' : 'remove';
  let routerMethod = options.replace ? 'replace' : 'push' ;
  document.body.classList[classMethod]('navigate-backward');
  component.props.router[routerMethod](uri);
};

export {setSwipe, resetSwipe, openPage};
