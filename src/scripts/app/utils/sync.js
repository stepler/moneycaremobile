import _ from 'lodash';
import PouchDB from 'pouchdb';
import * as zbase32 from './zbase32';
import {APP_HOST} from '../conf';

const REMOTE_DB_HOST = `http://${APP_HOST}/db`;

let remoteDb = null;

const AddSync = (syncKey) => {
  remoteDb = new PouchDB(`${REMOTE_DB_HOST}/${syncKey}`, {revs_limit:1});
};

const RemoveSync = () => {
  remoteDb = null;
};

const SyncWithRemoteDb = (localDb, callback) => {
  if (remoteDb && window.navigator.onLine) {
    localDb.sync(remoteDb)
      .on('error', () => {})
      .on('complete', () => {
        callback();
      });
  }
  else {
    callback();
  }
};

const getSyncKey = (rawSyncKey) => {
  const encodedKey = encodeURIComponent(rawSyncKey);
  const buffer = new Uint16Array(encodedKey.length);

  _.forEach(encodedKey, (c, idx) => {
    buffer[idx] = encodedKey.charCodeAt(idx);
  });

  return zbase32.encode(buffer);
};

export {
  AddSync,
  RemoveSync,
  SyncWithRemoteDb,
  getSyncKey,
};
