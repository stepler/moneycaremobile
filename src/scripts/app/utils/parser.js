
import _ from 'lodash';

const DAYSINWEEK = 7;

const setDefaultItem = (item) => {
  return _.defaults(item, {
    id: 0,
    sort: 0,
    depth: 0,
    title: '',
    money: 0,
    moneyChilds: 0,
    moneySpent: 0,
    isParent: false,
    // isEdit: false,
    isShow: true,
    isShowChilds: true,
    isDone: false
  })
};

const getWeeksBetweenDates = (dateStart, dateEnd) => {
  let prefixDays = ([6,0,1,2,3,4,5])[dateStart.getDay()];
  let tsStart = _.isObject(dateStart) ? dateStart.getTime() : dateStart ;
  let tsEnd = _.isObject(dateEnd) ? dateEnd.getTime() : dateEnd ;
  let days = Math.floor((tsEnd - tsStart) / (1000*60*60*24)) + 1;

  let daysByWeek = _.range(Math.floor((days+prefixDays) / DAYSINWEEK)).fill(DAYSINWEEK);
  let daysInLastWeek = Math.floor((days+prefixDays) % DAYSINWEEK);
  if (daysInLastWeek > 0) {
    daysByWeek.push( Math.floor((days+prefixDays) % DAYSINWEEK) );
  }
  if (daysByWeek.length > 1) {
    daysByWeek[0] -= prefixDays;
  }
  return [days, daysByWeek, tsStart];
};

const parseItemTitle = (rawTitle) => {

  let parseRawTitle = (value) => {
    value = _.trim(value);
    if (value.length > 0) {
      value = _.upperFirst(value);
    }
    return value;
  };

  let parseRawMoney = (value) => {
    let centsMatch = value.match(/[,\.](\d{1,2})$/) || ['', 0];
    let cents = parseInt(centsMatch[1]);
    if (centsMatch[0].length > 0) {
      value = value.substring(0, value.length-centsMatch[0].length)
    }
    if (value.length === 0 && cents === 0) {
      return 0;
      return [0, 0];
    }
    let dollars = parseInt(value.replace(/[^\d\-]/g, ''));
    return dollars;
    return [dollars, cents]
  };

  let moneyRaw = (rawTitle.match(/\-?[\d,\.]+$/) || [''])[0];
  let titleRaw = rawTitle.substring(0, rawTitle.length-moneyRaw.length);

  return {
    title: parseRawTitle(titleRaw),
    money: parseRawMoney(moneyRaw)
  }
};


const genId = () => {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
    return v.toString(16);
  });
};

const setSort = (itemsList) => {
  return itemsList.map((item, i) => {
    item.sort = i;
    return item;
  });
};

const setParents = (itemsList) => {
  return itemsList.map((item, i) => {
    let nextItem = itemsList[i+1];
    if (nextItem && nextItem.isEdit) {
      nextItem = itemsList[i+2];
    }
    item.isParent = nextItem ? item.depth < nextItem.depth : false;
    return item;
  });
};

const setDepth = (itemsList) => {
  var nextChange = {};
  return itemsList.map((item, i) => {
    let prevItem = itemsList[i-1];
    if (item.depth > 2) {
      item.depth = 2;
    }
    if (item.depth < 0) {
      item.depth = 0;
    }
    if (prevItem) {
      if (item.depth - prevItem.depth > 1) {
        item.depth = prevItem.depth + 1;
        nextChange[item.id] = item.depth;
      }
      else if (nextChange[prevItem.id] &&
           item.depth > prevItem.depth) {
        item.depth = prevItem.depth;
        nextChange[item.id] = item.depth;
      }
    }
    else {
      item.depth = 0;
    }
    if (item.depth === 0) {
      item.isShow = true;
    }
    return item;
  });
};

const setMoney = (itemsList) => {
  var dMoney = [0,0,0];
  return itemsList.reverse().map((item, i) => {
    if (item.isParent) {
      item.moneyChilds = dMoney[item.depth+1];
      dMoney[item.depth] += dMoney[item.depth+1];
      dMoney[item.depth+1] = 0;
    }
    else {
      dMoney[item.depth] += item.money;
    }
    return item;
  }).reverse();
};

const setMoneySpent = (itemsList, paymentsList) => {
  var dSpent = [0,0,0];
  return itemsList.reverse().map((item, i) => {
    if (item.isParent) {
      item.moneySpent = dSpent[item.depth+1];
      dSpent[item.depth] += dSpent[item.depth+1];
      dSpent[item.depth+1] = 0;
    }
    else {
      item.moneySpent = item.isDone
        ? item.money
        : _.sumBy((paymentsList[item.id] || []), 'money');
      dSpent[item.depth] += item.moneySpent
    }
    return item;
  }).reverse();
};

const setDone = (itemsList) => {
  var dDone = [true,true,true];
  return itemsList.reverse().map((item, i) => {
    if (item.isParent) {
      item.isDone = dDone[item.depth+1];
      dDone[item.depth+1] = true;
    }
    if (dDone[item.depth]) {
      dDone[item.depth] = item.isDone;
    }
    return item;
  }).reverse();
};

const normalizeItemsList = (itemsList) => {
  return (
    setDone(
      setMoney(
        setParents(
          setDepth(
            setSort(itemsList)
          )
        )
      )
    )
  );
};

const switchItems = (idx1, idx2, itemsList) => {
  let item = itemsList[idx1];
  itemsList[idx1] = itemsList[idx2];
  itemsList[idx2] = item;
  return itemsList;
};

const getChilds = (item, itemsList, maxDepth) => {
  maxDepth = maxDepth || 2;
  var idx = itemsList.indexOf(item);
  var valid = true;
  return itemsList.filter((d, i) => {
    if (i <= idx) { return false; }
    if (valid && item.depth >= d.depth) { valid = false; }
    if (valid && d.depth - item.depth > maxDepth) { return false; }
    return valid;
  });
};

const getLastChildIdx = (item, itemsList) => {
  const idx = itemsList.indexOf(item);
  const childs = getChilds(item, itemsList);

  return idx + childs.length;
};

const getParent = (item, itemsList) => {
  itemsList.reverse();
  var idx = itemsList.indexOf(item);
  var parentItem = null;
  itemsList.forEach((d, i) => {
    if (i <= idx) { return false; }
    if (!parentItem && item.depth > d.depth) { parentItem = d; }
  });
  itemsList.reverse();
  return parentItem;
};

const getNear = (item, itemsList) => {
  var parent = getParent(item, itemsList);
  if (parent) {
    return getChilds(parent, itemsList, 1);
  }
  return itemsList.filter((d) => { return d.depth === 0; })
};

const getIndexById = (itemId, itemsList) => {
  return _.findIndex(itemsList, {id: itemId});
};

const isDoneAll = (itemsList) => {
  return itemsList.every((d) => { return d.isDone; })
};


const AddItem = (itemId, itemsList, options) => {
  options = options || {};
  let item, idx, depth;

  if (_.isNull(itemId)) {
    idx = itemsList.length;
    item = setDefaultItem({
      id: genId(),
      sort: 0,
      depth: 0
    });
  }
  else {
    item = _.find(itemsList, {id: itemId});
    if (!item) {
      return itemsList;
    }

    idx = getLastChildIdx(item, itemsList);
    idx += 1;

    depth = options.sameDepth ? item.depth : item.depth + 1 ;
    item = setDefaultItem({
      id: genId(),
      sort: item.sort,
      depth: depth
    });
  }

  item = ParseItem(item);

  itemsList.splice(idx, 0, item);
  return normalizeItemsList(itemsList);
};

const UpdateItem = (itemId, rawTitle, itemsList, options) => {
  options = options || {};
  let item = _.find(itemsList, {id: itemId});

  if (!item) {
    return itemsList;
  }

  if (rawTitle === '') {
    return RemoveItem(itemId, itemsList);
  }

  let {title, money} = parseItemTitle(rawTitle);
  item.title = title;
  item.money = money;
  if (options.increaseDepth) {
    item.depth += 1;
  }
  if (options.decreaseDepth) {
    item.depth -= 1;
  }

  // let parent = getParent(item, itemsList);
  // if (parent) {
  //   item.isShow = parent.isShowChilds;
  // }

  item = ParseItem(item);
  return normalizeItemsList(itemsList);
};

const RemoveItem = (itemId, itemsList) => {
  let item = _.find(itemsList, {id: itemId});

  if (!item) {
    return itemsList;
  }

  var ids = [itemId].concat(
    getChilds(item, itemsList).map((i)=>{ return i.id })
  );
  itemsList = itemsList.filter((i)=>{ return ids.indexOf(i.id) === -1});
  return normalizeItemsList(itemsList);
};

const MoveUp = (itemId, itemsList) => {
  let idx1 = getIndexById(itemId, itemsList);
  let idx2 = idx1 - 1;
  if (idx2 < 0) {
    return itemsList;
  }
  return normalizeItemsList(switchItems(idx1, idx2, itemsList));
};

const MoveDown = (itemId, itemsList) => {
  let idx1 = getIndexById(itemId, itemsList);
  let idx2 = idx1 + 1;
  if (idx2 >= itemsList.length) {
    return itemsList;
  }
  return normalizeItemsList(switchItems(idx1, idx2, itemsList));
};

const DepthUp = (itemId, itemsList) => {
  let item = _.find(itemsList, {id: itemId});
  if (item) {
    item.depth -= 1;
  }
  return normalizeItemsList(itemsList);
};

const DepthDown = (itemId, itemsList) => {
  let item = _.find(itemsList, {id: itemId});
  if (item) {
    item.depth += 1;
  }
  return normalizeItemsList(itemsList);
};

const ShowItemChilds = (itemId, itemsList) => {
  let item = _.find(itemsList, {id: itemId});
  if (item) {
    item.isShowChilds = true;
    getChilds(item, itemsList, 1).forEach((d)=>{
      d.isShow = true;
      if (d.isParent && d.isShowChilds) {
        getChilds(d, itemsList, 1).forEach((d)=>{
          d.isShow = true;
        });
      }
    });
  }
  return normalizeItemsList(itemsList);
};

const HideItemChilds = (itemId, itemsList) => {
  let item = _.find(itemsList, {id: itemId});
  if (item) {
    item.isShowChilds = false;
    getChilds(item, itemsList).forEach((d)=>{
      d.isShow = false;
    });
  }
  return normalizeItemsList(itemsList);
};

const DoneItem = (itemId, itemsList) => {
  let item = _.find(itemsList, {id: itemId});
  if (item) {
    item.isDone = true;
  }
  return normalizeItemsList(itemsList);
};

const UndoneItem = (itemId, itemsList) => {
  let item = _.find(itemsList, {id: itemId});
  if (item) {
    item.isDone = false;
  }
  return normalizeItemsList(itemsList);
};

const ParseItem = (item) => {
  return {
    id: (item.id || genId()),
    sort: (parseInt(item.sort) || 0),
    depth: (parseInt(item.depth) || 0),
    title: (item.title || ''),
    money: (parseInt(item.money) || 0),
    moneyChilds: (parseInt(item.moneyChilds) || 0),
    moneySpent: (parseInt(item.moneySpent) || 0),
    isParent: !!item.isParent,
    isShow: !!item.isShow,
    isShowChilds: !!item.isShowChilds,
    isDone: !!item.isDone
  }
};


// const GenerateList = (sourceList) => {
//   let list = sourceList.map((item) => {
//     item.id = _.uniqueId('i');
//     return setDefaultItem(item);
//   });
//   list.sort((a, b) => { return a.sort - b.sort; });
//   return normalizeItemsList(list);
// }

const calcMoney = (list, param, options) => {
  options = options || {};
  let skipDone = !!options.skipDone;
  let skipUndone = !!options.skipUndone;
  return list.reduce((total, i) => {
    if (i.isParent ||
      (skipDone && i.isDone) ||
      (skipUndone && !i.isDone)){
      return total;
    }
    total += i[param];
    return total;
  }, 0)
};

const CalcTotalMoney = (list) => {
  return calcMoney(list, 'money')
};

const CalcSpentMoney = (list) => {
  return calcMoney(list, 'moneySpent')
};

const CalcFreeMoney = (incomingList, expensesList) => {
  return CalcTotalMoney(incomingList) - CalcTotalMoney(expensesList);
};

// const CalcSpentMoney = (incomingList, expensesList, moneyByWeek) => {
//   return CalcTotalMoney(incomingList)
//      - CalcTotalMoney(expensesList, {skipUndone:true})
//      - _.sum(moneyByWeek.map((w)=>{
//       return w.isPast ? w.money : 0 ;
//      }));
// }

const CalcMoneyByWeek = (money, dateStart, dateEnd) => {
  let [days, weeks, tsStart] = getWeeksBetweenDates(dateStart, dateEnd);
  var moneyByDay = money / days;
  var tsNow = (new Date).getTime();
  var tsEnd = 0;
  var tsPerDay = (1000*60*60*24);

  return weeks.map((days, i) => {
    tsEnd = tsStart + (days * tsPerDay);
    let isPast = tsNow > tsEnd;
    let isActive = tsNow < tsEnd && tsNow > tsStart;
    tsStart = tsEnd;
    return {
      week: (i+1),
      days: days,
      isPast: isPast,
      isActive: isActive,
      money: Math.round(moneyByDay * days)
    }
  });
};


const setDefaultPayment = (item) => {
  return _.defaults(item, {
    id: 0,
    title: '',
    money: 0,
  })
};

const AddPayment = (itemId, rawTitle, paymentsList) => {
  let {title, money} = parseItemTitle(rawTitle);
  let newPayment = setDefaultPayment({
    id: genId(),
    title: title,
    money: money
  });
  if (!_.has(paymentsList, itemId)) {
    paymentsList[itemId] = [];
  }
  paymentsList[itemId].unshift(newPayment);
  return paymentsList;
};

const RemovePayment = (itemId, paymentId, paymentsList) => {
  let list = _.reject((paymentsList[itemId] || []), {id:paymentId});
  if (list.length === 0) {
    delete paymentsList[itemId];
  }
  else {
    paymentsList[itemId] = list;
  }
  return paymentsList;
};

const NormalizeSpentList = (itemsList, paymentsList) => {
  return setMoneySpent(itemsList, paymentsList);
};

const NormalizePaymentsList = (paymentsList, incomingList, expensesList) => {
  var ids = [].concat(
    incomingList.map((i)=>{ return i.id }),
    expensesList.map((i)=>{ return i.id })
  );

  return _.reduce(paymentsList, (res, p, pId) => {
    if (ids.indexOf(pId) !== -1) {
      res[pId] = p;
    }
    return res;
  }, {});
};


const setDefaultDoc = (item) => {
  return _.defaults(item, {
    id: 0,
    sort: 0,
    start: 0,
    end: 0
  })
};

const normalizeDocsList = (docsList) => {
  docsList.sort((a, b) => { return b.sort - a.sort; });
  return docsList;
};

var lastDocId = null;

const GetLastDocId = () => {
  return lastDocId;
};

const AddDoc = (baseDoc, docsList) => {
  let newDoc = setDefaultDoc({
    id: genId(),
    sort: baseDoc.end.getTime(),
    start: baseDoc.start,
    end: baseDoc.end
  });
  docsList.push(newDoc);
  lastDocId = newDoc.id;
  return normalizeDocsList(docsList);
};

const UpdateDoc = (docId, updateDoc, docsList) => {
  let doc = _.find(docsList, {id: docId});
  if (!doc) {
    return docsList;
  }
  doc.sort = updateDoc.end.getTime();
  doc.start = updateDoc.start;
  doc.end = updateDoc.end;
  return normalizeDocsList(docsList);
};

const RemoveDoc = (docId, docsList) => {
  docsList = docsList.filter((i)=>{ return i.id !== docId});
  return normalizeDocsList(docsList);
};


export {
  NormalizeSpentList, NormalizePaymentsList,
  CalcMoneyByWeek, CalcFreeMoney, CalcSpentMoney, CalcTotalMoney,
  DoneItem, UndoneItem,
  ShowItemChilds, HideItemChilds,
  AddItem, UpdateItem, RemoveItem,
  MoveUp, MoveDown, DepthUp, DepthDown,
  AddPayment, RemovePayment,
  AddDoc, UpdateDoc, RemoveDoc,
  GetLastDocId
};
