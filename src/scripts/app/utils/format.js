
const moneyFormat = (value) => {
    value = value || 0;
    let sep = ' ';
    let dollars = value.toString();
    let minus = dollars[0] === '-';
    if (minus) { dollars = dollars.substr(1); }

    let prefix = (prefix = dollars.length) > 3 ? prefix % 3 : 0;

    return (minus ? '-' : '')
           + (prefix ? dollars.substr(0, prefix) + sep : "")
           + (dollars.substr(prefix).replace(/\-?(\d{3})(?=\d)/g, "$1" + sep))
};

export {moneyFormat};
