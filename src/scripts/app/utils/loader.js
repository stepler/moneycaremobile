import _ from 'lodash';
import PouchDB from 'pouchdb';
import {SyncWithRemoteDb} from './sync';

const dataDb = new PouchDB('mcaredb', {adapter: 'websql', revs_limit:1});
const settingsDb = new PouchDB('settings', {adapter: 'websql', revs_limit:1});

const LoadSettings = () => {
  return settingsDb.get('params')
  .then(
    (res) => { return res.data || {}; },
    () => { return {}; }
  );
};

const UpdateSettings = (data) => {
  let key = 'params';
  return settingsDb.get(key)
  .then(
    (res) => {
      return settingsDb.put({
        _id: key,
        _rev: res._rev,
        data: data
      });
    },
    () => {
      return settingsDb.put({
        _id: key,
        data: data
      });
    }
  );
};

const LoadDocsList = () => {
  return dataDb.get('doclist')
  .then(
    (res) => {
      let data = res.data || [];
      return data.map((doc) => {
        doc.end = new Date(doc.end);
        doc.start = new Date(doc.start);
        return doc;
      });
    },
    () => { return []; }
  );
};

const UpdateDocsList = (docsList) => {
  let listToUpdate = docsList.map((doc) => {
    doc = _.clone(doc);
    doc.start = doc.start.getTime();
    doc.end = doc.end.getTime();
    return doc;
  });

  return dataDb.get('doclist')
    .then(
      (res) => {
        return dataDb.put({
          _id: 'doclist',
          _rev: res._rev,
          data: listToUpdate
        });
      },
      () => {
        return dataDb.put({
          _id: 'doclist',
          data: listToUpdate
        });
      }
    )
    .then(SyncDocs);
};

const LoadDocsMeta = (id) => {
  return LoadDocsList()
  .then(
    (docsList) => {
      return _.find(docsList, {id}) || null;
    },
    () => { return {incoming:[], expenses:[]}; }
  );
};

const LoadDocsDetail = (id) => {
  let key = 'doc'+id;
  return dataDb.get(key)
  .then(
    (res) => {
      if (!_.has(res.data, 'payments')) {
        res.data.payments = {};
      }
      return res.data;
    },
    () => { return {incoming:[], expenses:[], payments:{}}; }
  );
};

const UpdateDocsDetail = (id, data) => {
  let key = 'doc'+id;
  return dataDb.get(key)
    .then(
      (res) => {
        return dataDb.put({
          _id: key,
          _rev: res._rev,
          data: data
        });
      },
      () => {
        return dataDb.put({
          _id: key,
          data: data
        });
      }
    )
    .then(SyncDocs);
};

const CloneDocsDetail = (sourceId, targetId) => {
  let key = 'doc'+targetId;
  LoadDocsDetail(sourceId)
    .then(
      (data) => {
        data.payments = {};
        return dataDb.put({
          _id: key,
          data: data
        });
      }
    )
    .then(SyncDocs);
};

const DeleteDocsDetail = (id) => {
  let key = 'doc'+id;
  return dataDb.get(key)
    .then(
      (res) => {
        return dataDb.remove({
          _id: key,
          _rev: res._rev
        })
      },
      () => {
        return true;
      }
    )
    .then(SyncDocs);
};

const SyncDocs = (res, callback) => {
  SyncWithRemoteDb(
    dataDb,
    callback || (() => {})
  );

  return res;
};

export {
  LoadSettings,
  UpdateSettings,
  LoadDocsList,
  LoadDocsMeta,
  LoadDocsDetail,
  UpdateDocsList,
  UpdateDocsDetail,
  CloneDocsDetail,
  DeleteDocsDetail,
  SyncDocs
};
