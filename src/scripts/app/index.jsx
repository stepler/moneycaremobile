'use strict';

import M  from 'moment';
import FC from 'fastclick';

import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, hashHistory } from 'react-router'

import {debug} from './utils/debug';
import Root from './pages/root';
import Main from './pages/main';
import Start from './pages/start';
import Doc from './pages/doc';
import NewDoc from './pages/new_doc';
import EditDoc from './pages/edit_doc';
import DesktopWarning from './pages/desktop_waring';


const isMobileDevice =
  // 'ontouchstart' in document.documentElement &&
  Math.min(window.innerWidth, window.innerHeight) < 450;

const isStandaloneMode =
  (window.matchMedia('(display-mode: standalone)').matches === true) ||
  ('standalone' in window.navigator && window.navigator.standalone === true);

const isIOS = /iPhone/i.test(navigator.userAgent) && !window.MSStream;
const isAndroid = /Android/i.test(navigator.userAgent);
const isUnknown = !isIOS && !isAndroid;

M.locale('ru');
M.updateLocale('ru', {
  months: 'Января_Февраля_Марта_Апреля_Мая_Июня_Июля_Августа_Сентября_Октября_Ноября_Декабря'.split('_'),
});

FC.attach(document.body);


const renderWarning = () =>
  ReactDOM.render((
    <DesktopWarning />
  ), document.querySelector('#application'));

const renderStart = () =>
  ReactDOM.render((
    <Start
      iOS={isIOS}
      android={isAndroid}
    />
  ), document.querySelector('#application'));

const renderApplication = () =>
  ReactDOM.render((
    <Router history={hashHistory}>
      <Route path="/" component={Root}>
        <IndexRoute component={Main} />
        <Route path="/newdoc" component={NewDoc} />
        <Route path="/docs/:docId" component={Doc} />
        <Route path="/docs/:docId/edit" component={EditDoc} />
      </Route>
    </Router>
  ), document.querySelector('#application'));

(() => {
  if (
    debug('app') ||
    debug('start') ||
    debug('warning')
  ) {
    if (debug('warning')) {
      return renderWarning();
    }
    if (debug('start')) {
      return renderStart();
    }
    return renderApplication();
  }

  if (!isMobileDevice) {
    return renderWarning();
  }
  if (!isStandaloneMode && !isUnknown) {
    return renderStart();
  }
  return renderApplication();
})();


