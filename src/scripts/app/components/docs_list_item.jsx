import M from 'moment';
import _ from 'lodash';

import React, { PropTypes } from 'react';
import cx from 'classnames';

import {Icon} from './macros';
import {setSwipe, resetSwipe} from '../utils/tools';


module.exports = React.createClass({

  displayName: 'DocListItem',

  propTypes: {
    id: PropTypes.any.isRequired,
    end: PropTypes.any.isRequired,
    start: PropTypes.any.isRequired,
    onOpenDoc: PropTypes.func,
    onEditDoc: PropTypes.func,
    onRemoveDoc: PropTypes.func,
  },

  getDefaultProps: function () {
    return {
      end: {},
      start: {},
      onOpenDoc: ()=>{},
      onEditDoc: ()=>{},
      onRemoveDoc: ()=>{}
    }
  },

  getInitialState: function () {
    return {
      requstRemove: false
    }
  },

  componentDidMount: function() {
    setSwipe(this.refs.swipe);
  },

  isSameMonth: function () {
    return this.props.start.getMonth() === this.props.end.getMonth();
  },

  openDoc: function () {
    this.props.onOpenDoc(this.props.id);
  },

  editDoc: function () {
    this.props.onEditDoc(this.props.id);
  },

  removeDoc: function () {
    this.props.onOpenDoc(this.props.id);
  },

  requstRemove: function () {
    this.setState({
      requstRemove: true
    });
  },

  removeItem: function() {
    this.props.onRemoveDoc(this.props.id);
  },

  discardRemove: function () {
    resetSwipe(this.refs.swipe);
    this.setState({
      requstRemove: false
    });
  },

  render: function () {
    let P = this.props;
    let S = this.state;
    return (
      <div className="swipe">
        <div ref="swipe" className="swipe--block">
          <button
            className="swipe--item doc btn--hidden"
            onClick={this.openDoc}>
            <Icon id="calendar" className="doc--calendar-icon" />
            <span className="doc--info">
              { this.isSameMonth() ? (
                <span className="doc--dates">
                  <span>{M(P.start).format('DD')}</span>
                  &nbsp;&mdash;&nbsp;
                  <span>{M(P.end).format('DD MMMM')}</span>
                </span>
              ) : (
                <span className="doc--dates">
                  <span>{M(P.start).format('DD MMMM')}</span>
                  &nbsp;&mdash;&nbsp;
                  <span>{M(P.end).format('DD MMMM')}</span>
                </span>
              )}
            </span>
            <Icon id="chevron-right" className="doc--next-icon" />
          </button>
          <div className="swipe--item doc-item--settings">
            {(S.requstRemove) ? (
              <div className="doc-item--settings-block doc-item--remove-request">
                <Icon id="question" className="doc-item--remove-request-icon" />
                <span className="doc-item--remove-request-title">Удалить?</span>
                <button
                  className="btn--hidden doc-item--remove-request-btn"
                  onClick={this.removeItem}>
                  <Icon id="check-o" className="doc-item--icon" />
                </button>
                <button
                  className="btn--hidden doc-item--remove-request-btn"
                  onClick={this.discardRemove}>
                  <Icon id="close-o" className="doc-item--icon" />
                </button>
              </div>
            ) : (
              <div className="doc-item--settings-block">
                <button
                  className="btn--hidden doc-item--settings-item"
                  onClick={this.editDoc}>
                  <Icon id="pencil" className="doc-item--icon" />
                </button>
                <button
                  className="btn--hidden doc-item--settings-item"
                  onClick={this.requstRemove}>
                  <Icon id="trash" className="doc-item--icon" />
                </button>
              </div>
            )}
          </div>
        </div>
      </div>
    )
  }
});
