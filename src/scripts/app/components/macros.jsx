
import React, { PropTypes } from 'react';
import cx from 'classnames';

const Icon = React.createClass({

    displayName: 'Icon',

    propTypes: {
        id: PropTypes.string,
        className: PropTypes.string
    },

    render: function () {
        return (
            <svg className={cx(['icon', this.props.className])}>
                <use xlinkHref={'#'+this.props.id}></use>
            </svg>
        )
    }
});

const Logo = React.createClass({

    displayName: 'Logo',

    propTypes: {
        size: PropTypes.string.isRequired
    },

    getDefaultProps: function() {
        return {
            size: 'normal'
        }
    },

    render: function () {
        return (
            <div className={cx(['logo', 'logo_'+this.props.size])}>
                <Icon id="logo" className="logo--pic" />
                <span className="logo--title"><i>money</i><b>care</b></span>
            </div>
        )
    }
});


export {Icon, Logo};
