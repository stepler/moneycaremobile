import M from 'moment';
import _ from 'lodash';

import React, { PropTypes } from 'react';
import cx from 'classnames';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

import DocItem from './doc_item';
import {Icon, Logo} from './macros';
import {moneyFormat} from '../utils/format';
import {
  CalcTotalMoney,
  CalcSpentMoney,
  AddItem, UpdateItem, RemoveItem,
  DoneItem, UndoneItem,
  ShowItemChilds, HideItemChilds,
  MoveUp, MoveDown, DepthUp, DepthDown,
  AddPayment, RemovePayment,
} from '../utils/parser';

module.exports = React.createClass({

  displayName: 'DocGroup',

  propTypes: {
    type: PropTypes.string.isRequired,
    itemsList: PropTypes.array.isRequired,
    paymentsList: PropTypes.object,
    onUpdateList: PropTypes.func.isRequired,
    onUpdatePayments: PropTypes.func
  },

  getDefaultProps: function() {
    return {
      itemsList: [],
      paymentsList: {},
      onUpdatePayments: ()=>{}
    }
  },

  getInitialState: function () {
    return {
      itemsList: [],
      paymentsList: {}
    }
  },

  componentWillMount: function() {
    this.updateList(this.props.itemsList, {skipSave: true});
    this.updatePayments(this.props.paymentsList, {skipSave: true});
  },

  componentWillReceiveProps: function(nextProps) {
    this.updateList(nextProps.itemsList, {skipSave: true});
    this.updatePayments(nextProps.paymentsList, {skipSave: true});
  },

  updateList: function(itemsList, options) {
    options = options || {};

    let moneyTotal = CalcTotalMoney(itemsList);
    let moneySpent = CalcSpentMoney(itemsList);
    this.setState({itemsList, moneyTotal, moneySpent});

    if (!options.skipSave) {
      this.props.onUpdateList(itemsList);
    }
  },

  addRootItem: function() {
    this.updateList(
      AddItem(null, this.state.itemsList),
      {skipSave: true}
    );
  },

  addItem: function(docId, options) {
    this.updateList(
      AddItem(docId, this.state.itemsList, options),
      {skipSave: true}
    );
  },

  removeItem: function(docId) {
    this.updateList(
      RemoveItem(docId, this.state.itemsList)
    );
  },

  updateItem: function(docId, rawTitle, options) {
    this.updateList(
      UpdateItem(docId, rawTitle, this.state.itemsList, options)
    );
  },

  updateItemPosition: function(docId, attr, offset) {
    let method;
    if (attr === 'sort' && offset < 0) { method = MoveUp; }
    if (attr === 'sort' && offset > 0) { method = MoveDown; }
    if (attr === 'depth' && offset < 0) { method = DepthUp; }
    if (attr === 'depth' && offset > 0) { method = DepthDown; }
    if (method) {
      this.updateList(
        method(docId, this.state.itemsList)
      );
    }
  },

  updateItemCheck: function(docId, status) {
    let method = status === true ? DoneItem : UndoneItem ;
    this.updateList(
      method(docId, this.state.itemsList)
    );
  },

  updatePayments: function(paymentsList, options) {
    options = options || {};

    this.setState({paymentsList});

    if (!options.skipSave) {
      this.props.onUpdatePayments(paymentsList);
    }
  },

  addPayment: function(itemId, rawTitle) {
    this.updatePayments(
      AddPayment(itemId, rawTitle, this.state.paymentsList)
    );
  },

  removePayment: function(itemId, paymentId) {
    this.updatePayments(
      RemovePayment(itemId, paymentId, this.state.paymentsList)
    );
  },

  toggleItemChilds: function(docId, status) {
    let method = status === 'show' ? ShowItemChilds : HideItemChilds ;
    this.updateList(
      method(docId, this.state.itemsList)
    );
  },

  getShownItems: function() {
    return this.state.itemsList.filter((i) => {
      return i.isShow;
    })
  },

  getPaymentsList: function(itemId) {
    return this.state.paymentsList[itemId] || null;
  },

  isEmptyItems: function() {
    return this.state.itemsList.length == 0;
  },

  renderMoney: function() {
    var {moneyTotal, moneySpent} = this.state;
    if (moneySpent === 0 || moneySpent === moneyTotal) {
      return moneyFormat(moneyTotal);
    }
    return '(' +
      moneyFormat(moneyTotal - moneySpent) +
      ') ' + moneyFormat(moneyTotal)
  },

  render: function () {
    let P = this.props;
    let S = this.state;

    return (
      <section className="list">
        <div className="separator-big">
          <span className="separator--title">
            {P.type === 'incoming' ? 'Доходы' : 'Расходы' }
          </span>
          {(!this.isEmptyItems()) ? (
            <span
              className="separator--plus"
              onClick={this.addRootItem}>
              <Icon id="plus" className="separator--plus-icon" />
            </span>
          ) : (void 0)}
          {(!this.isEmptyItems()) ? (
            <span className="separator--info">
              {this.renderMoney()}
              <Icon id="rouble" className="rouble15" />
            </span>
          ) : (void 0)}
        </div>
        <div>
          {(this.isEmptyItems()) ? (
            <div className="doc-item--stub">
              <button
                className="btn--hidden doc-item--stub-btn"
                onClick={this.addRootItem}>
                <Icon id="plus-o" className="doc-item--stub-icon" />
              </button>
            </div>
          ) : (
            <ReactCSSTransitionGroup
              component="div"
              transitionName="doc-item"
              transitionEnterTimeout={250}
              transitionLeaveTimeout={250}>
              {this.getShownItems().map((item) => {return(
                <DocItem
                  key={item.id}
                  type={P.type}
                  id={item.id}
                  sort={item.sort}
                  depth={item.depth}
                  title={item.title}
                  money={item.money}
                  moneyChilds={item.moneyChilds}
                  moneySpent={item.moneySpent}
                  isParent={item.isParent}
                  isDone={item.isDone}
                  isShowChilds={item.isShowChilds}
                  paymentsList={this.getPaymentsList(item.id)}
                  onAddItem={this.addItem}
                  onUpdateItem={this.updateItem}
                  onRemoveItem={this.removeItem}
                  onUpdateItemCheck={this.updateItemCheck}
                  onToggleItemChilds={this.toggleItemChilds}
                  onUpdateItemPosition={this.updateItemPosition}
                  onAddPayment={this.addPayment}
                  onRemovePayment={this.removePayment}
                />
              )})}
            </ReactCSSTransitionGroup>
          )}
        </div>
      </section>
    )
  }
});
