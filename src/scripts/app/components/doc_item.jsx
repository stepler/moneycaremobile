import _ from 'lodash';

import React, { PropTypes } from 'react';
import cx from 'classnames';

import {Icon, Logo} from './macros';
import {moneyFormat} from '../utils/format';
import {setSwipe, resetSwipe} from '../utils/tools';

const KEY_ENTER = 13;
const INCREASE_DEPTH = '.';
const DECREASE_DEPTH = '..';


module.exports = React.createClass({

  displayName: 'Item',

  propTypes: {
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    sort: PropTypes.number.isRequired,
    depth: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    money: PropTypes.number.isRequired,
    moneyChilds: PropTypes.number.isRequired,
    moneySpent: PropTypes.number.isRequired,
    isDone: PropTypes.bool.isRequired,
    isParent: PropTypes.bool.isRequired,
    isShowChilds: PropTypes.bool.isRequired,
    paymentsList: PropTypes.any,
    onAddItem: PropTypes.func.isRequired,
    onUpdateItem: PropTypes.func.isRequired,
    onRemoveItem: PropTypes.func.isRequired,
    onUpdateItemCheck: PropTypes.func.isRequired,
    onToggleItemChilds: PropTypes.func.isRequired,
    onUpdateItemPosition: PropTypes.func.isRequired,
    onAddPayment: PropTypes.func.isRequired,
    onRemovePayment: PropTypes.func.isRequired,
  },

  getDefaultProps: function() {
    return {
      id: 0,
      sort: 0,
      depth: 0,
      title: '',
      money: 0,
      moneyChilds: 0,
      moneySpent: 0,
      isDone: false,
      isParent: false,
      isShowChilds: true,
      paymentsList: null,
      onAddItem: ()=>{},
      onUpdateItem: ()=>{},
      onRemoveItem: ()=>{},
      onUpdateItemCheck: ()=>{},
      onToggleItemChilds: ()=>{},
      onUpdateItemPosition: ()=>{},
      onAddPayment: ()=>{},
      onRemovePayment: ()=>{},
    }
  },

  getInitialState: function () {
    return {
      isNew: false,
      editMode: false,
      requstRemove: false,
      isShowPayments: false,
    }
  },

  componentDidMount: function() {
    setSwipe(this.refs.swipe);
  },

  componentWillMount: function() {
    this.update(this.props);
  },

  componentWillReceiveProps: function(nextProps) {
    this.update(nextProps);
  },

  update: function(props) {
    this.setState({
      isNew: props.title === ''
    });
  },

  getItemValue: function(options) {
    options = options || {};
    if (!this.refs.item) {
      return '';
    }

    var value = (this.refs.item.value || '').trim();
    if (!options.raw) {
      value = _.trimEnd(value, INCREASE_DEPTH);
      value = _.trimEnd(value, DECREASE_DEPTH);
    }

    return value;
  },

  getItemOptions: function() {
    var options = {
      increaseDepth: false,
      decreaseDepth: false,
    };
    var value = this.getItemValue({raw:true});

    if (value.slice(-2) === DECREASE_DEPTH) {
      options.decreaseDepth = true;
    }
    else if (value.slice(-1) === INCREASE_DEPTH) {
      options.increaseDepth = true;
    }

    return options;
  },

  addItem: function(options) {
    this.props.onAddItem(this.props.id, options);
  },

  removeItem: function() {
    this.props.onRemoveItem(this.props.id);
  },

  updateItem: function() {
    let value = this.getItemValue();
    let options = this.getItemOptions();
    this.props.onUpdateItem(this.props.id, value, options);

    if (value !== '' &&
      this.state.editMode === false) {
      this.addItem({sameDepth: true});
    }
    this.setState({editMode:false});
    document.activeElement.blur();
  },

  updateItemPosition: function(attr, offset) {
    this.props.onUpdateItemPosition(this.props.id, attr, offset);
  },

  toggleItemChilds: function() {
    let status = this.props.isShowChilds ? 'hide' : 'show';
    this.props.onToggleItemChilds(this.props.id, status);
  },

  toggleItemDone: function(e) {
    let status = !this.props.isDone;
    this.props.onUpdateItemCheck(this.props.id, status);
  },

  getPaymentValue: function() {
    if (!this.refs.payment) {
      return '';
    }

    return (this.refs.payment.value || '').trim();
  },

  resetPaymentValue: function() {
    if (this.refs.payment) {
      this.refs.payment.value = '';
    }
  },

  focusPaymentValue: function() {
    if (this.refs.payment) {
      this.refs.payment.focus();
    }
  },

  addPayment: function() {
    var value = this.getPaymentValue();
    if (value !== '') {
      this.props.onAddPayment(this.props.id, value);
      this.resetPaymentValue();
      this.focusPaymentValue();
    }
  },

  removePayment: function(paymentId) {
    this.props.onRemovePayment(this.props.id, paymentId);
    this.focusPaymentValue();
  },

  ctrlInput: function(type, e) {
    let keyCode = e.nativeEvent.keyCode;
    let hasCtrl = e.nativeEvent.ctrlKey || e.nativeEvent.metaKey;

    if (keyCode === KEY_ENTER) {
      if (type === 'item') {
        this.updateItem();
      }
      if (type === 'payment') {
        this.addPayment();
      }
      return this.ctrlInputPrevent(e);
    }
  },

  ctrlInputPrevent: function(e) {
    e.preventDefault();
    e.stopPropagation();
    return false;
  },

  requestAdd: function() {
    this.addItem();
    resetSwipe(this.refs.swipe);
  },

  requestEdit: function() {
    resetSwipe(this.refs.swipe);
    this.setState({
      editMode: true
    });
  },

  requstRemove: function() {
    this.setState({
      requstRemove: true
    });
  },

  discardRemove: function() {
    resetSwipe(this.refs.swipe);
    this.setState({
      requstRemove: false
    });
  },

  renderToEdit: function() {
    if (this.props.title === '') {
      return '';
    }
    if (this.props.money > 0) {
      return `${this.props.title} ${this.props.money}`;
    }
    return this.props.title;
  },

  togglePaymentsShow: function() {
    var {isShowPayments} = this.state;
    this.setState({
      isShowPayments: !isShowPayments
    });
  },

  hasPayments: function() {
    return !this.props.isParent && this.props.paymentsList;
  },

  renderMoney: function() {
    var {money, moneyChilds, moneySpent, isParent} = this.props;
    var moneyTotal = isParent ? moneyChilds : money ;
    if (moneySpent === 0 || moneySpent === moneyTotal) {
      return moneyFormat(moneyTotal);
    }
    return '(' +
      moneyFormat(moneyTotal - moneySpent) +
      ') ' + moneyFormat(moneyTotal)
  },

  render: function () {
    let P = this.props;
    let S = this.state;

    return (
      <div className={cx('list--item doc-item',{
        done: P.isDone
      })}>
        <div className="swipe">
          <div
            ref="swipe"
            className="swipe--block">
            <div className="swipe--item doc-item--main">
              <div className={cx(['doc-item--l'+(P.depth+1)], {
                parent: P.isParent
              })}>
                {(S.isNew || S.editMode) ? (
                  <div className="doc-item--content doc-item--add">
                    <input
                      ref="item"
                      type="text"
                      autoFocus={S.isNew}
                      onKeyUp={this.ctrlInput.bind(this, 'item')}
                      className="doc-item--add-input"
                      defaultValue={this.renderToEdit()} />
                    <button
                      className="btn--hidden"
                      onClick={this.updateItem}>
                      <Icon id={S.isNew ? 'plus-o' : 'check-o'} className="doc-item--add-icon" />
                    </button>
                  </div>
                ) : (
                  <div className="doc-item--content">
                    {(P.isParent) ? (
                      <button
                        className={cx('doc-item--btn', {
                          open: P.isShowChilds
                        })}
                        onClick={this.toggleItemChilds}>
                        {(P.depth > 0) ? (
                          <Icon id={P.isShowChilds ? 'chevron-down' : 'chevron-right' } className="doc-item--icon doc-item--icon_status" />
                        ) : (
                          <Icon id="archive" className="doc-item--icon doc-item--icon_status" />
                        )}
                      </button>
                    ) : (
                      <button
                        className={cx('doc-item--btn', {
                          open: P.isShowChilds
                        })}
                        onClick={this.toggleItemDone}>
                        <Icon id="check-o" className="doc-item--icon doc-item--icon_status" />
                      </button>
                    )}
                    <span
                      className="doc-item--title"
                      onClick={P.isParent ? this.toggleItemChilds : this.toggleItemDone }
                    >{P.title}</span>
                    <button
                      className={cx([
                        'doc-item--btn',
                        'doc-item--money'
                      ],{
                        open: P.isShowPayments
                      })}
                      onClick={this.togglePaymentsShow}>
                      {this.renderMoney()}
                      <Icon id="rouble" className={P.depth === 2 ? 'rouble13' : 'rouble15' } />
                    </button>
                  </div>
                )}
              </div>
            </div>
            <div className="swipe--item doc-item--settings">
              {(S.requstRemove) ? (
                <div className={cx(['doc-item--l'+(P.depth+1)])}>
                  <div className="doc-item--content doc-item--remove-request">
                    <Icon id="question" className="doc-item--remove-request-icon" />
                    <span className="doc-item--remove-request-title">Удалить?</span>
                    <button
                      className="btn--hidden doc-item--remove-request-btn"
                      onClick={this.removeItem}>
                      <Icon id="check-o" className="doc-item--icon" />
                    </button>
                    <button
                      className="btn--hidden doc-item--remove-request-btn"
                      onClick={this.discardRemove}>
                      <Icon id="close-o" className="doc-item--icon" />
                    </button>
                  </div>
                </div>
              ) : (
                <div className="doc-item--content">
                  <button
                    className="btn--hidden doc-item--settings-item"
                    onClick={this.requestAdd}>
                    <Icon id="plus-o" className="doc-item--icon" />
                  </button>
                  <button
                    className="btn--hidden doc-item--settings-item"
                    onClick={this.requestEdit}>
                    <Icon id="pencil" className="doc-item--icon" />
                  </button>
                  <button
                    className="btn--hidden doc-item--settings-item"
                    onClick={this.requstRemove}>
                    <Icon id="trash" className="doc-item--icon" />
                  </button>
                </div>
              )}
            </div>
          </div>
        </div>
        {!P.isParent && S.isShowPayments ? (
          <div className="doc-item--payments">
            <div className={cx(['doc-item--l'+(P.depth+1)])}>
              <div className="doc-item--payment">
                <div className="doc-item--content doc-item--add">
                  <input
                    ref="payment"
                    type="text"
                    autoFocus={true}
                    onKeyUp={this.ctrlInput.bind(this, 'payment')}
                    className="doc-item--add-input" />
                  <button
                    className="btn--hidden"
                    onClick={this.updateItem}>
                    <Icon id="plus-o" className="doc-item--add-icon" />
                  </button>
                </div>
              </div>
              {P.paymentsList ? (
                P.paymentsList.map((payment) => {return(
                  <div key={payment.id}>
                    <div
                      className="doc-item--payment">
                      <div className="doc-item--content">
                        <Icon id="tag" className="doc-item--payment-icon doc-item--payment-icon_main" />
                        <span className="doc-item--payment-title">{payment.title}</span>
                        <span className="doc-item--payment-money">
                          {moneyFormat(payment.money)}
                          <Icon id="rouble" className={P.depth === 2 ? 'rouble11' : 'rouble13' } />
                        </span>
                        <button
                          className="btn--hidden doc-item--payment-btn"
                          onClick={this.removePayment.bind(this, payment.id)}>
                          <Icon id="trash" className="doc-item--payment-icon" />
                        </button>
                      </div>
                    </div>
                  </div>
                )})
              ) : (void 0)}
            </div>
          </div>
        ) : (void 0)}
      </div>
    )
  }
});
