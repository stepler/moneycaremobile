
import React from 'react';


module.exports = React.createClass({

  displayName: 'Screen',

  hasMenu: function() {
    var first = this.props.children[0];
    if (!first) { return false; }
    return first.type.displayName === 'Menu';
  },

  getMenu: function() {
    if (!this.hasMenu()) { return []; }
    return [ this.props.children[0] ];
  },

  getHeader: function() {
    var idx = this.hasMenu() ? 1 : 0;
    var header = this.props.children[idx];
    return header ? [header] : [] ;
  },

  getContent: function() {
    var idx = this.hasMenu() ? 2 : 1;
    return this.props.children.slice(idx);
  },

  render: function() {
    return (
      <div className="screen theme_bg">
        {this.getMenu()}
        <div className="screen--header theme_bg">
          <div className="screen--header-block">
            {this.getHeader()}
          </div>
        </div>
        <div className="screen--content">
          {this.getContent()}
        </div>
      </div>
    )
  }
});
