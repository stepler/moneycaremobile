import M from 'moment';
import _ from 'lodash';

import React, { PropTypes } from 'react';
import cx from 'classnames';

import {Icon} from './macros';
import {setSwipe, resetSwipe} from '../utils/tools';
import DocsListItem from './docs_list_item';


module.exports = React.createClass({

  displayName: 'DocsList',

  propTypes: {
    list: PropTypes.array,
    onOpenDoc: PropTypes.func,
    onCreateDoc: PropTypes.func,
    onEditDoc: PropTypes.func,
    onRemoveDoc: PropTypes.func
  },

  getDefaultProps: function() {
    return {
      list: [],
      onOpenDoc: ()=>{},
      onCreateDoc: ()=>{},
      onRemoveDoc: ()=>{}
    }
  },

  getInitialState: function () {
    return {
      yearsList: []
    }
  },

  componentWillMount: function() {
    this.update(this.props);
  },

  componentWillReceiveProps: function(nextProps) {
    this.update(nextProps);
  },

  update: function(props) {
    this.setState({
      yearsList: this.splitDocsByYears(props.list)
    });
  },

  openDoc: function(docId) {
    this.props.onOpenDoc(docId);
  },

  createDoc: function() {
    this.props.onCreateDoc();
  },

  editDoc: function(docId) {
    this.props.onEditDoc(docId);
  },

  removeDoc: function(docId) {
    this.props.onRemoveDoc(docId);
  },

  isEmptyItems: function() {
    return this.state.yearsList.length == 0;
  },

  splitDocsByYears: function(docsList) {
    let docsByYears = _.groupBy(docsList, (doc) => {
      return (new Date(doc.sort)).getFullYear();
    });

    let years = _.keys(docsByYears);
    years.sort((a, b) => { return b - a; });

    return _.map(years, (year) => {
      let docsList = docsByYears[year];
      docsList.sort((a, b) => { return b.sort - a.sort; });
      return {
        title: year,
        docsList: docsList
      }
    });
  },

  render: function () {
    let S = this.state;
    return (
      <section className="list">
        {(this.isEmptyItems()) ? (
          <div className="doc-item--stub doc-item--stub_main">
            <button
              className="btn--hidden doc-item--stub-btn"
              onClick={this.createDoc}>
              <Icon id="plus-o" className="doc-item--stub-icon" />
              <span className="doc-item--stub-title">Добавь первый план =)</span>
            </button>
          </div>
        ) : (
          S.yearsList.map((year, i) => {return(
            <div key={year.title}>
              <div className="separator">{year.title}</div>
              {year.docsList.map((doc, i) => {return(
                <div
                  key={doc.id}
                  className="list--item">
                  <DocsListItem
                    id={doc.id}
                    end={doc.end}
                    start={doc.start}
                    onOpenDoc={this.openDoc}
                    onEditDoc={this.editDoc}
                    onRemoveDoc={this.removeDoc}
                  />
                </div>
              )})}
            </div>
          )})
        )}
      </section>
    )
  }
});
