import _ from 'lodash';

import React, { PropTypes } from 'react';
import cx from 'classnames';

import {Icon, Logo} from './macros';


module.exports = React.createClass({

  displayName: 'Header',

  propTypes: {
    title: PropTypes.string,
    leftBtn: PropTypes.string,
    rightBtn: PropTypes.string,
    onClick: PropTypes.func
  },

  getDefaultProps: function() {
    return {
      title: null,
      leftBtn: null,
      rightBtn: null,
      onClick: ()=>{}
    }
  },

  getInitialState: function() {
    return {
      leftBtnIcon: null,
      leftBtnText: null,
      rightBtnIcon: null,
      rightBtnText: null
    }
  },

  componentWillMount: function() {
    this.update(this.props);
  },

  update: function(props) {
    let leftBtn = (props.leftBtn || '').split('#');
    let rightBtn = (props.rightBtn || '').split('#');
    this.setState({
      leftBtnText: leftBtn[0] || '',
      leftBtnIcon: leftBtn[1] || '',
      rightBtnText: rightBtn[0] || '',
      rightBtnIcon: rightBtn[1] || '',
    });
  },

  click: function(btn) {
    this.props.onClick(btn);
  },

  render: function() {
    var P = this.props;
    var S = this.state;

    return (
      <header className="header">
        <div className="header--block">
          {P.leftBtn ? (
            <button
              className="header--back"
              onClick={this.click.bind(this, 'left')}>
              {S.leftBtnIcon.length ? (
                <Icon
                  id={S.leftBtnIcon}
                  className={'header--back-icon header--icon_'+S.leftBtnIcon} />
              ) : (void 0)}
              {S.leftBtnText.length ? (
                <span className="header--back-title">{S.leftBtnText}</span>
              ) : (void 0)}
            </button>
          ) : (void 0)}
          <h1 className="header--title">
            {P.title ? (
              P.title
            ) : (
              <div className="header--logo">
                <Icon id="logo" className="header--logo-icon" />
                <span className="header--logo-title">
                  <span>money</span>
                  <b>care</b>
                </span>
              </div>
            )}
          </h1>
          {P.rightBtn ? (
            <button
              className="header--next"
              onClick={this.click.bind(this, 'right')}>
              {S.rightBtnIcon.length ? (
                <Icon
                  id={S.rightBtnIcon}
                  className={'header--next-icon header--icon_'+S.rightBtnIcon} />
              ) : (void 0)}
              {S.rightBtnText.length ? (
                <span className="header--next-title">{S.rightBtnText}</span>
              ) : (void 0)}
            </button>
          ) : (void 0)}
        </div>
      </header>
    )
  }
});
