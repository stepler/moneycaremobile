import M from 'moment';
import _ from 'lodash';

import React, { PropTypes } from 'react';
import cx from 'classnames';

import {Icon, Logo} from './macros';
import {moneyFormat} from '../utils/format';
import {
  CalcFreeMoney, CalcMoneyByWeek, CalcRemainMoney
} from '../utils/parser';

module.exports = React.createClass({

  displayName: 'DocSubheader',

  propTypes: {
    incoming: PropTypes.array.isRequired,
    expenses: PropTypes.array.isRequired
  },

  getDefaultProps: function() {
    return {
      incoming: [],
      expenses: [],
    }
  },

  getInitialState: function () {
    return {
      freeMoney: 0
    }
  },

  componentWillMount: function() {
    this.update(this.props);
  },

  componentWillReceiveProps: function(nextProps) {
    this.update(nextProps);
  },

  update: function(props, options) {
    options = options || {};
    let P = this.props;

    let freeMoney = CalcFreeMoney(P.incoming, P.expenses);

    // let moneyByWeek = CalcMoneyByWeek(freeMoney, P.doc.start, P.doc.end);
    // let remainMoney = CalcRemainMoney(P.incoming, P.expenses, moneyByWeek);

    this.setState({freeMoney});
  },

  render: function () {
    let P = this.props;
    let S = this.state;

    return (
      <section className="subheader">
        <span>
          {moneyFormat(S.freeMoney)}
          <Icon id="rouble" className="rouble15" />
        </span>
      </section>
    )
  }
});
