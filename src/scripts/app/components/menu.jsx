import React, { PropTypes } from 'react';
import cx from 'classnames';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

import {Icon} from './macros';
import {
  LoadSettings,
  UpdateSettings,
  SyncDocs
} from '../utils/loader';
import {
  AddSync,
  RemoveSync,
  getSyncKey
} from '../utils/sync';


module.exports = React.createClass({

  displayName: 'Menu',

  propTypes: {
    isOpen: PropTypes.bool,
    onClose: PropTypes.func
  },

  getDefaultProps: function() {
    return {
      onClose: ()=>{},
      onSyncAdd: ()=>{}
    }
  },

  getInitialState: function() {
    return {
      syncKey: null,
      isSyncRequested: false,
      activeTheme: 'blue',
      themes: [
        'blue', 'green', 'red',
        'brown', 'grey', 'dpurple'
      ],
    }
  },

  componentWillMount: function () {
    LoadSettings()
    .then((settings) => {
      const state = {};
      if (settings.theme) {
        state.activeTheme = settings.theme
      }
      if (settings.syncKey) {
        AddSync(settings.syncKey);
        state.syncKey = settings.syncKey;
      }
      this.setState(state);
    });
  },

  setTheme: function(theme) {
    LoadSettings()
    .then((settings) => {
      settings.theme = theme;
      UpdateSettings(settings);
      this.setState({activeTheme: theme});
      localStorage.setItem('theme', theme);
      document.querySelector('html').className = theme;
    });
  },

  setSync: function(syncKey) {
    LoadSettings()
    .then((settings) => {
      settings.syncKey = syncKey;
      UpdateSettings(settings);
      this.setState({syncKey});

      if (syncKey) {
        AddSync(syncKey);
        SyncDocs(null, () => {
          this.props.onSyncAdd();
        });
      }
      else {
        RemoveSync();
      }
    });
  },

  close: function() {
    this.props.onClose();
  },

  requestSync: function() {
    this.setState({isSyncRequested: true})
  },

  removeSync: function() {
    this.setSync(null);
  },

  addSync: function() {
    const {value} = this.refs.syncInput;

    if (value.length > 0) {
      this.setSync(getSyncKey(value));
      this.setState({isSyncRequested: false});
    }
  },

  render: function() {
    const P = this.props;
    const S = this.state;

    return (
      <ReactCSSTransitionGroup
        component="div"
        transitionName="menu"
        transitionEnterTimeout={250}
        transitionLeaveTimeout={250}
      >
        {P.isOpen ? (
          <section className="menu">
            <div className="theme_bg menu--screen">
              <div className="menu--header header">
                <div className="header--block">
                  <h2 className="header--title">Настройки</h2>
                  <button
                    onClick={this.close}
                    className="header--next">
                    <Icon id="close" className="menu--close-icon" />
                  </button>
                </div>
              </div>
              <div className="menu--section">
                <h3 className="menu--section-title">Синхронизация</h3>
                <div className="menu--sync">
                  {!S.syncKey && !S.isSyncRequested && (
                    <div className="menu--sync-block">
                      <button
                        className="btn--hidden menu--sync-btn"
                        onClick={this.requestSync}>
                        <Icon id="plus-o" className="menu--sync-btn-icon_big" />
                      </button>
                    </div>
                  )}
                  {!S.syncKey && S.isSyncRequested && (
                    <div className="menu--sync-block">
                      <div className="menu--sync-confirm">
                        <input
                          autoFocus={true}
                          ref="syncInput"
                          type="text"
                          className="menu--sync-confirm-input"
                        />
                        <button
                          className="btn--hidden"
                          onClick={this.addSync}>
                          <Icon id="check-o" className="menu--sync-confirm-icon" />
                        </button>
                      </div>
                    </div>
                  )}
                  {Boolean(S.syncKey) && (
                    <div className="menu--sync-block">
                      <button
                        className="btn--hidden menu--sync-btn"
                        onClick={this.removeSync}>
                        {'Синхронизация включена'}
                        <Icon id="close" className="menu--sync-btn-icon" />
                      </button>
                    </div>
                  )}
                </div>
              </div>
              <div className="menu--section">
                <h3 className="menu--section-title">Темы</h3>
                <div className="menu--themes">
                  {S.themes.map((theme) => {return(
                    <button
                      key={theme}
                      onClick={this.setTheme.bind(this, theme)}
                      className={cx(['menu--themes-item', theme], {
                        'active': S.activeTheme === theme
                      })}>
                      <span className={cx(['menu--themes-preview', theme+'-theme_bg'])}></span>
                    </button>
                  )})}
                </div>
              </div>
            </div>
          </section>
        ) : (void 0)}
      </ReactCSSTransitionGroup>
    )
  }
});
