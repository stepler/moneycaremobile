
import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'


module.exports = ({ children, location }) => (
  <ReactCSSTransitionGroup
    component="div"
    transitionName="screen"
    transitionEnterTimeout={250}
    transitionLeaveTimeout={250}
  >
    {React.cloneElement(children, {
      key: location.pathname
    })}
  </ReactCSSTransitionGroup>
);
