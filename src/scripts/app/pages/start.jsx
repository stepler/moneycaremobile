
import _ from 'lodash';

import React, { PropTypes } from 'react';
import cx from 'classnames';

import {Icon, Logo} from '../components/macros';


module.exports = React.createClass({

  displayName: 'Start',

  propTypes: {
    iOS: PropTypes.bool,
  },

  getDefaultProps: function() {
    return {
      iOS: false,
    }
  },

  renderIOS: function() {
    return (
      <div className="start_ios">
        <div className="start--logo">
          <Icon id="logo" className="start--logo-icon" />
        </div>
        <div className="start--block">
          <h1 className="start--title">Запуск приложения</h1>
          <p className="start--message">
            <span>Для запуска приложения</span>
            <span>нажмите на кпопку  <Icon id="share-apple" className="start--message-icon_share" /> и выбрите</span>
            <span>&laquo;Добавить&nbsp;на&nbsp;домашний&nbsp;экран&raquo;.</span>
          </p>
        </div>
      </div>
    )
  },

  renderAndroid: function() {
    return (
      <div className="start_android">
        <div className="start--block">
          <h1 className="start--title">Запуск приложения</h1>
          <p className="start--message">
            <span>Для запуска приложения</span>
            <span>нажмите&nbsp;на&nbsp;кпопку&nbsp;<Icon id="more" className="start--message-icon_more" />&nbsp;и&nbsp;выбрите</span>
            <span>&laquo;Добавить&nbsp;на&nbsp;главный&nbsp;экран&raquo;.</span>
          </p>
        </div>
        <div className="start--logo">
          <Icon id="logo" className="start--logo-icon" />
        </div>
      </div>
    )
  },

  render: function () {
    var S = this.state;
    var P = this.props;

    return (
      <div className="start blue-theme_bg">
        {P.iOS ? (
          this.renderIOS()
        ) : (
          this.renderAndroid()
        )}
      </div>
    )
  }
});
