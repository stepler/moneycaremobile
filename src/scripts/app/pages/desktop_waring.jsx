
import React, { PropTypes } from 'react';
import cx from 'classnames';

import {Icon, Logo} from '../components/macros';


module.exports = React.createClass({

  displayName: 'DesktopWarning',

  render: function () {
    var S = this.state;
    var P = this.props;

    return (
      <div className="d-warn blue-theme_bg">
        <div className="d-warn--block">
          <div className="d-warn--logo">
            <Icon id="logo" className="d-warn--logo-icon" />
          </div>
          <h1 className="d-warn--title">Ой...</h1>
          <p className="d-warn--message">
            <span>Приложение не оптимизировано для декстопов / планшетов.</span>
            <span>Да, печально, но вот так...</span>
            <span>Пожалуйста, откройте приложение со смартфона.</span>
          </p>
        </div>
      </div>
    )
  }
});
