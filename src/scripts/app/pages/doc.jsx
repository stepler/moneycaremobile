
import M from 'moment';
import _ from 'lodash';

import React, { PropTypes } from 'react';
import cx from 'classnames';
import withRouter from 'react-router/lib/withRouter'

import Screen from '../components/screen';
import Header from '../components/header';
import DocsList from '../components/docs_list';
import DocGroup from '../components/doc_group';
import DocSubheader from '../components/doc_subheader';
import {
  LoadDocsMeta,
  LoadDocsDetail,
  UpdateDocsDetail
} from '../utils/loader';
import {
  NormalizeSpentList,
  NormalizePaymentsList
} from '../utils/parser';
import {openPage} from '../utils/tools';



module.exports = withRouter(React.createClass({

  displayName: 'Doc',

  propTypes: {},

  getInitialState: function () {
    return {
      meta: null,
      detail: null
    }
  },

  getId: function() {
    return this.props.params.docId;
  },

  componentWillMount: function () {
    Promise.all([
      LoadDocsMeta(this.getId()),
      LoadDocsDetail(this.getId())
    ])
    .then(([meta, detail]) => {
      this.setState({detail, meta});
    });
  },

  onHeaderClick: function (btn) {
    if (btn === 'left') {
      openPage(this, '/', {backward:true});
    }
  },

  updateDocsList: function(docsList) {
    this.setState({docsList});
    this.tryLoadLastDoc();
    this.props.params.docId;
  },

  getList: function(type) {
    return this.state.detail[type];
  },

  getPaymentsList: function() {
    return this.state.detail.payments;
  },

  updateList: function(type, list) {
    let detail = this.state.detail;
    if (type === 'incoming') {
      detail.incoming = list;
    }
    if (type === 'expenses') {
      detail.expenses = list;
    }
    if (type === 'payments') {
      detail.payments = list;
    }

    detail.expenses = NormalizeSpentList(detail.expenses, detail.payments);
    detail.incoming = NormalizeSpentList(detail.incoming, detail.payments);
    detail.payments = NormalizePaymentsList(detail.payments, detail.incoming, detail.expenses);

    this.setState({detail});

    UpdateDocsDetail(this.getId(), detail);
  },

  isSameMonth: function(doc) {
    return doc.start.getMonth() === doc.end.getMonth();
  },

  getTitle: function() {
    var meta = this.state.meta;
    if (!meta) {
      return '...';
    }
    if (this.isSameMonth(meta)) {
      return M(meta.start).format('DD — ')
           + M(meta.end).format('DD MMMM');
    }
    return M(meta.start).format('DD MMM — ')
         + M(meta.end).format('DD MMM');
  },

  render: function () {
    let S = this.state;

    return (
      <Screen>
        <Header
          title={this.getTitle()}
          leftBtn="#chevron-left"
          onClick={this.onHeaderClick}
        />
        {(S.detail) ? (
          <DocSubheader
            incoming={S.detail.incoming}
            expenses={S.detail.expenses}
          />
        ) : (void 0)}
        {(S.detail) ? (
          <div>
            <DocGroup
              type="incoming"
              itemsList={S.detail.incoming}
              onUpdateList={this.updateList.bind(this, 'incoming')}
            />
            <DocGroup
              type="expenses"
              itemsList={S.detail.expenses}
              paymentsList={this.getPaymentsList()}
              onUpdateList={this.updateList.bind(this, 'expenses')}
              onUpdatePayments={this.updateList.bind(this, 'payments')}
            />
          </div>
        ) : (void 0)}
      </Screen>
    )
  }
}));
