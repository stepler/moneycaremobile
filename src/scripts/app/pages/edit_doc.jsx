
import _ from 'lodash';
import M from 'moment';

import React, { PropTypes } from 'react';
import cx from 'classnames';
import withRouter from 'react-router/lib/withRouter'

import Screen from '../components/screen';
import Header from '../components/header';
import {UpdateDoc} from '../utils/parser';
import {openPage} from '../utils/tools';
import {LoadDocsMeta, LoadDocsList, UpdateDocsList} from '../utils/loader';


module.exports = withRouter(React.createClass({

  displayName: 'EditDoc',

  getInitialState: function () {
    return {
      end: 0,
      start: 0,
      focus: '',
      docsList: []
    }
  },

  componentWillMount: function() {
    Promise.all([
      LoadDocsMeta(this.getId()),
      LoadDocsList()
    ])
    .then(([meta, docsList]) => {
      let end = M(meta.end);
      let start = M(meta.start);
      this.setState({docsList, end, start});
    });
  },

  getId: function() {
    return this.props.params.docId;
  },

  onHeaderClick: function(btn) {
    openPage(this, '/', {backward:true});
  },

  setFocus: function(val) {
    this.setState({focus: val});
  },

  handleChange: function (attr, e) {
    let state = {};
    let value = M(e.target.value, "YYYY-MM-DD");
    if (!value.isValid()) {
      return;
    }

    state[attr] = value;
    if (attr === 'start' && this.state.end.isBefore(value)) {
      state['end'] = value.clone().add(1, 'days');
    }
    if (attr === 'end' && this.state.start.isAfter(value)) {
      state['start'] = value.clone().subtract(1, 'days');
    }
    this.setState(state);
  },

  updateDoc: function() {
    let S = this.state;

    let docsList = UpdateDoc(this.getId(), {
      start: S.start.toDate(),
      end: S.end.toDate()
    }, S.docsList);

    UpdateDocsList(docsList)
    .then(() => {
      this.onHeaderClick('left');
    });
  },

  render: function () {
    let S = this.state;
    let P = this.props;

    return (
      <Screen>
        <Header
          title="Редактировать план"
          leftBtn="#chevron-left"
          onClick={this.onHeaderClick}
        />
        {(S.end && S.start) ? (
          <section className="form">
            <div className="form--item">
              <span className={cx(["form--input"],{
                'focus': S.focus === 'start'
              })}>
                {S.start ? (
                  S.start.format('DD MMMM YYYY')
                ) : (
                  'От'
                )}
                <input
                  type="date"
                  className="form--input_hidden"
                  onBlur={this.setFocus.bind(this, '')}
                  onFocus={this.setFocus.bind(this, 'start')}
                  value={S.start && S.start.format('YYYY-MM-DD')}
                  onChange={this.handleChange.bind(this, 'start')} />
              </span>
            </div>
            <div className="form--item">
              <span className={cx(["form--input"],{
                'focus': S.focus === 'end'
              })}>
                {S.end ? (
                  S.end.format('DD MMMM YYYY')
                ) : (
                  'До'
                )}
                <input
                  type="date"
                  className="form--input_hidden"
                  onBlur={this.setFocus.bind(this, '')}
                  onFocus={this.setFocus.bind(this, 'end')}
                  value={S.end && S.end.format('YYYY-MM-DD')}
                  onChange={this.handleChange.bind(this, 'end')} />
              </span>
            </div>
            <div className="form--item">
              <button
                className="form--button"
                onClick={this.updateDoc}
                >Сохранить</button>
            </div>
          </section>
        ) : (void 0)}
      </Screen>
    )
  }
}));
