
import _ from 'lodash';

import React, { PropTypes } from 'react';
import cx from 'classnames';
import withRouter from 'react-router/lib/withRouter'

import {
  LoadDocsList,
  UpdateDocsList,
  DeleteDocsDetail
} from '../utils/loader';
import {openPage} from '../utils/tools';
import Menu from '../components/menu';
import Screen from '../components/screen';
import Header from '../components/header';
import DocsList from '../components/docs_list';


module.exports = withRouter(React.createClass({

  displayName: 'Main',

  propTypes: {},

  getInitialState: function () {
    return {
      docsList: [],
      isMenuOpen: false,
      // isLoaded: false
    }
  },

  componentWillMount: function () {
    LoadDocsList()
    .then((docsList) => {
      this.setState({docsList});
    });
  },

  onSyncAdd: function () {
    LoadDocsList()
      .then((docsList) => {
        this.setState({docsList});
      });
  },

  onHeaderClick: function (btn) {
    if (btn === 'right') {
      this.createDoc();
    }
    if (btn === 'left') {
      this.toggleMenu();
    }
  },

  toggleMenu: function () {
    this.setState({isMenuOpen: !this.state.isMenuOpen});
  },

  createDoc: function() {
    openPage(this, '/newdoc');
  },

  openDoc: function (docId) {
    openPage(this, '/docs/'+docId);
  },

  editDoc: function (docId) {
    openPage(this, '/docs/'+docId+'/edit');
  },

  // openPage: function (uri, options) {
  //   options = options || {}
  //   let method = options.replace ? 'replace' : 'push' ;
  //   this.props.router[method]( uri );
  // },

  removeDoc: function (docId) {
    let docsList = _.reject(this.state.docsList, {id: docId});
    Promise.all([
      DeleteDocsDetail(docId),
      UpdateDocsList(docsList)
    ])
    .then(() => {
      this.setState({docsList});
    });
  },

  updateDocsList: function (docsList) {
    this.setState({docsList});
    this.tryLoadLastDoc();
  },

  render: function () {
    var S = this.state;
    var P = this.props;

    return (
      <Screen>
        <Menu
          isOpen={S.isMenuOpen}
          onClose={this.toggleMenu}
          onSyncAdd={this.onSyncAdd}
        />
        <Header
          rightBtn="#plus"
          leftBtn="#navicon"
          onClick={this.onHeaderClick}
        />
        <DocsList
          list={S.docsList}
          onOpenDoc={this.openDoc}
          onCreateDoc={this.createDoc}
          onEditDoc={this.editDoc}
          onRemoveDoc={this.removeDoc}
        />
      </Screen>
    )
  }
}));
