
import _ from 'lodash';
import M from 'moment';

import React, { PropTypes } from 'react';
import cx from 'classnames';
import withRouter from 'react-router/lib/withRouter'

import Screen from '../components/screen';
import Header from '../components/header';
import {openPage} from '../utils/tools';
import {AddDoc, GetLastDocId} from '../utils/parser';
import {
  LoadDocsList,
  UpdateDocsList,
  CloneDocsDetail
} from '../utils/loader';


module.exports = withRouter(React.createClass({

  displayName: 'NewDoc',

  getInitialState: function () {
    return {
      end: 0,
      start: 0,
      source: undefined,
      docsList: [],
      focus: '',
    }
  },

  componentWillMount: function() {
    this.update(this.props);
  },

  update: function(docsList) {
    LoadDocsList()
    .then((docsList) => {
      let {start, end} = this.getDefaultDates(docsList);
      let source = (docsList[0] || {}).id || undefined;
      this.setState({docsList, start, end, source});
    });
  },

  getDefaultDates: function(docsList) {
    let doc = docsList[0];

    if (!doc || !this.isNearMonth(doc)) {
      return {
        start: M({h:0,m:0,s:0,ms:0}).startOf('month'),
        end:   M({h:0,m:0,s:0,ms:0}).endOf('month')
      }
    }

    let start = M(doc.end).add(1, 'days');
    let end = start.clone();

    if (this.isSame(M(doc.end).endOf('month'), M(doc.end)) &&
      this.isSame(M(doc.start).startOf('month'), M(doc.start))) {
      end.endOf('month');
    }
    else {
      let diff = Math.abs(M(doc.start).diff(doc.end, 'days'));
      end.add(diff, 'days');
    }

    return {start, end};
  },

  isNearMonth: function(last) {
    return Math.abs(M(last).diff((new Date), 'months')) <= 2;
  },

  isSame: function(d1, d2) {
    return d1.format('DDMMYYY') === d2.format('DDMMYYY');
  },

  handleChange: function (attr, e) {
    let state = {};
    let value = e.target.value;

    if (attr === 'end' ||
        attr === 'start') {
      value = M(value, "YYYY-MM-DD");
      if (!value.isValid()) {
        return;
      }
    }

    state[attr] = value;
    if (attr === 'start' && this.state.end.isBefore(value)) {
      state['end'] = value.clone().add(1, 'days');
    }
    if (attr === 'end' && this.state.start.isAfter(value)) {
      state['start'] = value.clone().subtract(1, 'days');
    }
    this.setState(state);
  },

  createDoc: function() {
    let S = this.state;
    let sourceDocId = S.source;

    let docsList = AddDoc({
      start: S.start.toDate(),
      end: S.end.toDate()
    }, S.docsList);

    Promise.all([
      UpdateDocsList(docsList),
      (sourceDocId ? CloneDocsDetail(sourceDocId, GetLastDocId()) : null)
    ])
    .then(() => {
      this.onHeaderClick('left');
    });
  },

  onHeaderClick: function(btn) {
    openPage(this, '/', {backward:true});
  },

  setFocus: function(val) {
    this.setState({focus: val});
  },

  isSameMonth: function (doc) {
    return doc.start.getMonth() === doc.end.getMonth();
  },

  renderSourceValue: function(doc) {
    let to = M(doc.end).format('DD MMMM YYYY');
    let from = M(doc.start).format(this.isSameMonth(doc) ? 'DD' : 'DD MMMM');
    return `${from}&nbsp;&mdash;&nbsp;${to}`;
  },

  render: function () {
    let S = this.state;
    let P = this.props;

    return (
      <Screen>
        <Header
          title="Создать план"
          leftBtn="#chevron-left"
          onClick={this.onHeaderClick}
        />
        <section className="form">
          <div className="form--item">
            <span className={cx(["form--input"],{
              'focus': S.focus === 'start'
            })}>
              {S.start ? (
                S.start.format('DD MMMM YYYY')
              ) : (
                'От'
              )}
              <input
                type="date"
                className="form--input_hidden"
                onBlur={this.setFocus.bind(this, '')}
                onFocus={this.setFocus.bind(this, 'start')}
                value={S.start && S.start.format('YYYY-MM-DD')}
                onChange={this.handleChange.bind(this, 'start')} />
            </span>
          </div>
          <div className="form--item">
            <span className={cx(["form--input"],{
              'focus': S.focus === 'end'
            })}>
              {S.end ? (
                S.end.format('DD MMMM YYYY')
              ) : (
                'До'
              )}
              <input
                type="date"
                className="form--input_hidden"
                onBlur={this.setFocus.bind(this, '')}
                onFocus={this.setFocus.bind(this, 'end')}
                value={S.end && S.end.format('YYYY-MM-DD')}
                onChange={this.handleChange.bind(this, 'end')} />
            </span>
          </div>
          <div className="form--item">
            <span className={cx(["form--input"],{
              'focus': S.focus === 'source'
            })}>
              {S.source ? (
                <span dangerouslySetInnerHTML={{
                  __html: this.renderSourceValue(_.find(S.docsList, {id:S.source}))
                }}></span>
              ) : (
                'Новый план'
              )}
              <select
                className="form--input_hidden"
                onBlur={this.setFocus.bind(this, '')}
                onFocus={this.setFocus.bind(this, 'source')}
                onChange={this.handleChange.bind(this, 'source')}
                value={S.source}>
                <option value="">Новый план</option>
                {S.docsList.map((doc, i) => {return(
                  <option
                    key={doc.id}
                    value={doc.id}
                    dangerouslySetInnerHTML={{__html: this.renderSourceValue(doc)}}>
                  </option>
                )})}
              </select>
            </span>
          </div>
          <div className="form--item">
            <button
              className="form--button"
              onClick={this.createDoc}
              >Создать</button>
          </div>
        </section>
      </Screen>
    )
  }
}));
