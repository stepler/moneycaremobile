
'use strict';

const fs = require('fs');
const path = require('path');

const gulp = require('gulp');
const less = require('gulp-less');
const gulpif = require('gulp-if');
const watch = require('gulp-watch');
const cssmin = require('gulp-cssmin');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const replace = require('gulp-replace');
const plumber = require('gulp-plumber');
const datauri = require('datauri').sync;
const webpack = require('webpack-stream');


const NODE_ENV = process.env.NODE_ENV || 'production';
const isProduction = NODE_ENV === 'production';
let isWatchEnabled = false;

const DEST_DIR = path.join(__dirname, 'dist'),
    TMP_DIR = path.join(__dirname,  '.tmp'),
    SRC_DIR = path.join(__dirname, 'src'),
    RES_DIR = SRC_DIR+'/res',
    SCRIPTS_DIR = SRC_DIR+'/scripts',
    STYLES_DIR = SRC_DIR+'/styles';


gulp.task('app:scripts', () => {
  const config = require('./webpack.config.js');

  return gulp
    .src(SCRIPTS_DIR+'/app/index.jsx')
    .pipe(plumber())
    .pipe(webpack(config))
    .pipe(gulpif(isProduction, uglify()))
    .pipe(gulp.dest(DEST_DIR+'/static/build'));
});


gulp.task('app:styles', () => {
  return gulp
    .src(STYLES_DIR+'/app/index.less')
    .pipe(plumber())
    .pipe(less())
    .pipe(gulpif(isProduction, cssmin()))
    .pipe(rename('styles.css'))
    .pipe(gulp.dest(DEST_DIR+'/static/build'));
});

gulp.task('app:fonts', () => {
  const FONTS_DEST = DEST_DIR+'/static/fonts';

  return gulp
    .src(RES_DIR+'/fonts/*')
    .pipe(plumber())
    .pipe(gulp.dest(FONTS_DEST));
});

gulp.task('app:appcache', () => {
  return gulp
    .src(SRC_DIR+'/manifest.appcache')
    .pipe(plumber())
    .pipe(gulpif(!isProduction, rename('manifest.appcache.dev')))
    .pipe(gulp.dest(DEST_DIR));
});

gulp.task('app', ['app:fonts', 'app:scripts', 'app:styles', 'app:appcache']);


gulp.task('html:scripts', () => {
  return gulp
    .src(SCRIPTS_DIR+'/bootstrap.js')
    .pipe(plumber())
    .pipe(gulpif(isProduction, uglify()))
    .pipe(gulp.dest(TMP_DIR));
});


gulp.task('html:styles', () => {
  return gulp
    .src(STYLES_DIR+'/bootstrap.less')
    .pipe(plumber())
    .pipe(less())
    .pipe(gulpif(isProduction, cssmin()))
    .pipe(gulp.dest(TMP_DIR));
});


gulp.task('html', ['html:scripts', 'html:styles'], () => {
  const favicon = datauri(RES_DIR+'/favicon.png');
  const icons_sprite = fs.readFileSync(RES_DIR+'/icons_sprite.svg', {encoding:'utf8'});
  const bootstrap_styles = fs.readFileSync(TMP_DIR+'/bootstrap.css', {encoding:'utf8'});
  const bootstrap_scripts = fs.readFileSync(TMP_DIR+'/bootstrap.js', {encoding:'utf8'});

  return gulp
    .src(SRC_DIR+'/index.html')
    .pipe(plumber())
    .pipe(replace('%favicon%', favicon))
    .pipe(replace('%icons_sprite%', icons_sprite))
    .pipe(replace('%bootstrap_styles%', bootstrap_styles))
    .pipe(replace('%bootstrap_scripts%', bootstrap_scripts))
    .pipe(gulp.dest(DEST_DIR));
    // .pipe(replace(/\s*manifest=".*?"/, ''))
    // .pipe(rename('offline.html'))
    // .pipe(gulp.dest(DEST_DIR))
});


gulp.task('watch', ['html'], () => {
  isWatchEnabled = true;
  gulp.start('app:fonts');
  gulp.start('app:scripts');
  watch(SRC_DIR+'/index.html', () => { gulp.start('html') });
  watch(RES_DIR+'/favicon.png', () => { gulp.start('html') });
  watch(RES_DIR+'/icons/*.svg', () => { gulp.start('html') });
  watch(SCRIPTS_DIR+'/bootstrap.js', () => { gulp.start('html') });
  watch(STYLES_DIR+'/bootstrap.less', () => { gulp.start('html') });
  watch(STYLES_DIR+'/app/**/*.less', () => { gulp.start('app:styles') });
  watch(SRC_DIR+'/manifest.appcache', () => { gulp.start('app:appcache') });
});


gulp.task('build', ['html', 'app']);

gulp.task('dev', ['watch']);
