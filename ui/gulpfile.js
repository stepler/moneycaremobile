
'use strict';

const path = require('path');

const gulp = require('gulp');
const filter = require('gulp-filter');
const rename = require('gulp-rename');
const replace = require('gulp-replace');
const plumber = require('gulp-plumber');
const svgSprite = require('gulp-svg-sprite');
const imageResize = require('gulp-image-resize');


const allowedIcons = [
  'logo',
  'logo-bold',
  'rouble',
  'load-d',
  'ios-more',
  'ei-spinner',
  'ei-plus',
  'ei-plus-o',
  'ei-close',
  'ei-close-o',
  'ei-calendar',
  'ei-chevron-down',
  'ei-chevron-left',
  'ei-chevron-right',
  'ei-check-o',
  'ei-pencil',
  'ei-archive',
  'ei-trash',
  'ei-tag',
  'ei-question',
  'ei-navicon',
  'ei-share-apple'
];


const DEST_DIR = path.join(__dirname, '..', 'src', 'res');
const SRC_DIR = path.join(__dirname, 'resources');

gulp.task('app:icons', () => {
  const ICON_DEST = DEST_DIR+'/icons';

  return gulp
    .src(SRC_DIR+'/appicon.png')
    .pipe(plumber())
    .pipe(imageResize({width:152,height:152}))
    .pipe(rename('76x76@2x.png'))
    .pipe(gulp.dest(ICON_DEST))
    .pipe(imageResize({width:120,height:120}))
    .pipe(rename('60x60@2x.png'))
    .pipe(gulp.dest(ICON_DEST))
    .pipe(imageResize({width:76,height:76}))
    .pipe(rename('76x76@1x.png'))
    .pipe(gulp.dest(ICON_DEST))
    .pipe(imageResize({width:60,height:60}))
    .pipe(rename('60x60@1x.png'))
    .pipe(gulp.dest(ICON_DEST));
});

gulp.task('app:splash', () => {
  const SPLASH_DEST = DEST_DIR+'/splash';

  return gulp
    .src(SRC_DIR+'/splash.png')
    .pipe(plumber())
    .pipe(imageResize({width:640,height:960}))
    .pipe(rename('320x480@2x.png'))
    .pipe(gulp.dest(SPLASH_DEST))
    .pipe(imageResize({width:320,height:480}))
    .pipe(rename('320x480@1x.png'))
    .pipe(gulp.dest(SPLASH_DEST));
});

gulp.task('html:icons', () => {
  const config = {
    mode: {
      symbol: {
        dest: '',
        prefix: 'svg.%s',
        sprite: 'icons_sprite.svg',
        render: {
          css: false
        }
      }
    }
  };
  const iconsFilter = (file) => {
    return allowedIcons.indexOf(path.basename(file.path, '.svg')) >= 0
  };
  return gulp
    .src(SRC_DIR+'/icons/*.svg')
    .pipe(plumber())
    .pipe(filter(iconsFilter))
    .pipe(svgSprite(config))
    .pipe(replace(/<\?xml.*?>/, ''))
    .pipe(replace(/<\!DOC.*?>/, ''))
    .pipe(replace(/id="ei-/g, 'id="'))
    .pipe(replace(/id="ios-/g, 'id="'))
    .pipe(gulp.dest(DEST_DIR));
});

gulp.task('build', [
  'app:icons',
  'app:splash',
  'html:icons',
]);
