const log4js = require('log4js');

const {
  LOGS_PATH,
  IS_PRODUCTION,
} = require('../config');

const layout = {
  type: 'pattern',
  pattern: '%d{yyyy-MM-dd hh:mm:ss} [%p] %m',
};

log4js.configure({
  appenders: {
    file: {
      type: 'dateFile',
      filename: `${LOGS_PATH}/app.log`,
      pattern: '.yyyy-MM',
      alwaysIncludePattern: true,
      layout,
    },
    console: {
      type: 'console',
      layout,
    },
  },
  categories: {
    default: {
      appenders: [
        IS_PRODUCTION
          ? 'file'
          : 'console',
      ],
      level: IS_PRODUCTION
        ? 'info'
        : 'debug',
    },
  },
});

global.logger = log4js.getLogger();
