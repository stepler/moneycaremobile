
exports.LOGS_PATH = '/logs';

exports.PROJECT_SOURCE = 'https://api.bitbucket.org/2.0/repositories/stepler/moneycaremobile/downloads';

exports.PROJECT_DEST = '/www';

exports.IS_PRODUCTION = process.env.NODE_ENV === 'production';

exports.ACCESS_TOKEN = process.env.ACCESS_TOKEN;

exports.ACCESS_USER = 'stepler';
