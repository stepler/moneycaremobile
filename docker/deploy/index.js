require('./bootstrap/logger');

const CronJob = require('cron').CronJob;

const getIsNeedUpdate = require('./libs/checker');
const updateProject = require('./libs/updater');

new CronJob('0 * * * * *', async () => {
  try {
    logger.info('Check release task');

    const isNeedUpdate = await getIsNeedUpdate();

    if (isNeedUpdate) {
      logger.info('Update project');
      await updateProject();
    }
  }
  catch (e) {
    logger.error(e.message);
  }

}, null, true, 'Europe/Moscow');
