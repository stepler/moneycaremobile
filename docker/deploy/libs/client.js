const {URL} = require('url');
const request = require('request');

const {
  PROJECT_SOURCE,
  ACCESS_TOKEN,
  ACCESS_USER
} = require('../config');

const HTTP_OK = 200;

const getUrl = () => {
  const url = new URL(PROJECT_SOURCE);
  url.username = ACCESS_USER;
  url.password = ACCESS_TOKEN;
  return url.toString();
};

const getRelease = () =>
  new Promise((resolve, reject) => {
    request(getUrl(), {json: true}, (err, res, body) => {
      if (
        !err &&
        res.statusCode === HTTP_OK
      ) {
        resolve(body);
      }
      else {
        if (err) {
          reject(error.stack);
        }
        else {
          reject(`Unable to load release - ${res.statusCode}`)
        }
      }
    });
  })
    .then((res) => res.values[0]);


const getReleaseDataStream = (releaseName) =>
  request(`${getUrl()}/${releaseName}`);

module.exports = {
  getRelease,
  getReleaseDataStream
};
