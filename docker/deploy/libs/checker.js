const client = require('./client');

let lastRelease = '';

module.exports = async () => {
  const release = await client.getRelease();

  if (release && release.created_on !== lastRelease) {
    lastRelease = release.created_on;
    return true;
  }

  return false;
};
