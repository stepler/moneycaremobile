const tar = require('tar');
const del = require('del');

const client = require('./client');

const {PROJECT_DEST} = require('../config');

module.exports = async () => {
  const release = await client.getRelease();

  if (release) {
    del.sync([`${PROJECT_DEST}/**`], {force: true});
    client.getReleaseDataStream(release.name)
      .pipe(tar.x({strip: 1, cwd: PROJECT_DEST}));
  }
};
