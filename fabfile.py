#! coding:utf-8
import os
import uuid
import time

from fabric.api import *
from fabric import utils
from fabtools import nodejs, files, python

NODE_MODULES_DIR = "node_modules"
ROOT_PATH = "/root"
PROJECT_PATH = "/root/moneycare"
PROJECT_BRANCH = "HEAD"

env.roledefs["prod"] = ["root@77.244.214.161"]

@task
@roles("prod")
def deploy():
  deploy_key = prompt('Set deploy key:')
  tmp_file = "/tmp/%s.tar.gz" % uuid.uuid4().hex

  local("tar -czf {output} . -C docker".format(output=tmp_file))

  put(tmp_file, tmp_file)

  run("mkdir -p {project_path}".format(project_path=PROJECT_PATH))

  run("tar -xzf {filename} -C {project_path}".\
    format(project_path=PROJECT_PATH, filename=tmp_file))

  run("rm -f {filename}".format(filename=tmp_file))
  local("rm -f {filename}".format(filename=tmp_file))

  with cd(PROJECT_PATH):
    run("mv docker-compose-prod.yml docker-compose.yml")

    run("docker-compose rm --force --stop")
    run("ACCESS_TOKEN={token} docker-compose build".format(token=deploy_key))
    run("ACCESS_TOKEN={token} docker-compose up -d".format(token=deploy_key))
    time.sleep(5)
    run("docker ps -a")
