'use strict';

let webpack = require('webpack');

const isExternal = (module) => {
  return (module.userRequest + '')
    .indexOf('node_modules') >= 0;
};

module.exports = {
  context: __dirname + '/src',

  entry: {
    app: './scripts/app/index.jsx',
  },

  output: {
    filename: '[name].js',
  },

  resolve: {
    extensions: ['', '.js', '.jsx']
  },

  module: {
    loaders: [{
      test:   /\.jsx?$/,
      loader: 'babel',
      exclude: /(node_modules|bower_components)/,
      query: {
        plugins: ['lodash'],
        presets: ['es2015', 'react']
      }
    }]
  },

  plugins: [
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru|en\-gb/),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify(process.env.NODE_ENV),
        'APP_HOST': JSON.stringify(process.env.APP_HOST),
      }
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: (module) => {
        return isExternal(module);
      }
    })
  ],

  watch: process.env.NODE_ENV === 'development'
};
